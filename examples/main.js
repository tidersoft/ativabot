'use strict';
const BootBot = require('../lib/BootBot');
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const app = express();
const _ = require("underscore");
const https = require('http')

const GET_DB = function(path, resp){
	
	const options = {
  hostname: 'tidermondb.herokuapp.com',
  port: 80,
  path: path,
  method: 'GET',
   headers: {
    'Content-Type': 'application/json',
    'user': 'Wargamer',
	'db': 'apk'
  }
}

const req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`);

  res.on('data', data => {
	  resp(res.statusCode, data);
  });
});

req.on('error', error => {
  console.error(error);
});

req.end();
};

const data = "todo=buy milk"

const POST_DB = function(path, data, resp){
	
const options = {
  hostname: 'tidermondb.herokuapp.com',
  port: 80,
  path: path,
  method: 'POST',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Content-Length': data.length,
	'user': 'Wargamer',
	'db': 'apk'
  }
}

const req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', resp)
})

req.on('error', error => {
  console.error(error)
})

req.write(data)
req.end()
} 

const DROP_DB = function(path, resp){
	
	
const options = {
  hostname: 'tidermondb.herokuapp.com',
  port: 80,
  path: path+"?action=drop",
  method: 'DELETE',
  headers: {
	'user': 'root',
	'db': 'apk'
  }
}

const req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', resp)
})

req.on('error', error => {
  console.error(error)
})


req.end()
} 
const TRIM_DB = function(path, resp){
	
const options = {
  hostname: 'tidermondb.herokuapp.com',
  port: 80,
  path: path+"?action=trim",
  method: 'DELETE',
  headers: {
	'user': 'root',
	'db': 'apk'
  }
}

const req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', resp)
})

req.on('error', error => {
  console.error(error)
})

req.end()
} 

const echoModule = require('./modules/echo');


const bot = new BootBot({
  accessToken: process.env.APP_TOKEN,
  verifyToken: process.env.APP_ID,
  appSecret: process.env.APP_SECRET
});

function msleep(n) {
  Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n);
}
function sleep(n) {
  msleep(n*1000);
}

const context = (userId, text) => {
	bot.sendAction(userId, 'mark_seen');
	bot.conversation(userId,(convo) => {
		convo.set('cmd', text);
		convo.set('userId', userId);
		convo.set('cach', []);
		commands(convo);
	});
	
};

const commands = (convo) => {
	const text = convo.get('cmd');
	const userId = convo.get('userId');
	
		GET_DB("/Tidermoon/players/"+userId, (code, data) => {
			if(code == 200){
			console.log(data.toString());
				const param = fromJson(data.toString());
				console.log('params: '+param.toString());
				setPlayer(convo, param);
				
				if(text == 'status'){
					action(convo,'/Tidermoon/commands/status');
				}else{
					say(['Nie wiesz co robić?','Sprawdź mój samouczek na stronie.'], convo, noaction);
				}
				
			}else{
				say('Jesteś tu nowy?', convo, (convo) => {action(convo,'/Tidermoon/commands/login')});
				
			}
		});
		
};

bot.on('message', (payload, chat) => {
  const text = payload.message.text;
  const userId = payload.sender.id;
	context(userId, text);
});

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};


function setPlayer(convo , param){
	convo.set('name',  parseVal(param,'name'));
	convo.set('region',  parseVal(param,'region'));
	convo.set('age',  parseVal(param,'age'));
	convo.set('gender',  parseVal(param,'gender'));
	convo.set('rase',  parseVal(param,'rase'));
}

function say(text, convo, next){
	if(Array.isArray(text)){
		let sa = text.shift();
		if(text.length == 1)
			text = text[0];
		convo.say(sa.replaceAll('/n','\n'),{ typing: 1000 ,onRead: function(payload, chat, data){sleep(1.2);say(text, convo, next);}});
	}else{
		convo.say(text.replaceAll('/n','\n'),{ typing: 1000 ,onRead: function(payload, chat, data){sleep(1.2);next(convo);}});

	}
}



const toJson = (obj) => {
	return JSON.stringify(obj, function(key, value) {
	if (typeof value === "function") {
		return "/Function(" + value.toString() + ")/";
		}
		return value;
	});
};

const fromJson = (json) => {
	return JSON.parse(json, function(key, value) {
	if (typeof value === "string" &&
		value.startsWith("/Function(") &&
		value.endsWith(")/")) {
		value = value.substring(10, value.length - 2);
		return value;
	}
		return value;
	});
};

const commit = (convo , url) => {
	var arr = convo.get('cach');
	for(var i=0; i<arr.length; i++){
		var str = arr[i].Id+'='+arr[i].Value;
		POST_DB(url,str, d => {
			process.stdout.write('POST_DB: '+url+' '+str+'\n');
			process.stdout.write(d+'\n');
		});
	}
	
	noaction(convo);
}


const noaction = (convo) => {console.log(convo.get('cach')); convo.end();}

const action = (convo, url) => {
	
	GET_DB(url, (code, data) => {
			if(code == 200){
				console.log(data.toString());
				const param = fromJson(data.toString());
				console.log('params: '+param.toString());
				
				if(parseVal(param,'type') == 'say'){
					let text = findrep(parseVal(param,'say'), convo);
					say(text, convo, (convo) => {action(convo, parseVal(param,'next'));});
				}else
				
				if(parseVal(param,'type') == 'ask'){
					ask(convo, param);
				}else
					
				if(parseVal(param,'type') == 'askbutton'){
					askButtons(convo, param);
				}else
					
				if(parseVal(param,'type') == 'asknumber'){
					askNumber(convo, param);
				}else
				
				if(parseVal(param,'type') == 'commit'){
					let text = findrep(parseVal(param,'url'), convo);
					commit(convo, text);
				}else
				
				if(parseVal(param,'type') == 'end')
				{
					noaction(convo);
				}
				
				
			}else{
				say('Przepraszam jestem trochę zajęta. Spróbuj za moment.', convo, noaction);
				
			}
		});
};

function parseVal(param, id){
	return param.Values.find(x => x.Id === id).Value;
}

function parseNode(param, id){
	return param.Nodes.find(x => x.Id === id);
}

function findrep(str, convo){
	console.log(str);
	var pos = str.indexOf("${");
	if(pos > 0){
	var pos2 = str.indexOf("}");
	var name = str.substring(pos+2,pos2);
	console.log(name);
	return findrep(str.replace('${'+name+'}',convo.get(name)), convo);
	}
	return str;
}

function set(convo,set, text){
	if(set != ''){
		convo.set(set, text);
		var c = convo.get('cach');
		c.push({Id: set, Value: text});
		convo.set('cach',c);
	}
}


const ask = (convo, param) => {
	let ask = findrep(parseVal(param,'ask'), convo);
  convo.ask(ask, (payload, convo, data) => {
    const text = payload.message.text;
	set(convo, parseVal(param,'set'), text);
	let ans = findrep(parseVal(param,'ans'), convo);
    say(ans, convo, (convo) => {action(convo, parseVal(param,'next'));});
  },{ typing: 1000 });
};

const askNumber = (convo, param) => {
	let ask = findrep(parseVal(param,'ask'), convo);
  convo.ask(ask, (payload, convo, data) => {
    const text = payload.message.text;
	if(isNaN(text)){
		say(['To nie liczba!', 'Tym razem podaj prawidłową wartość.', 'OK?'], convo, (convo) => {askNumber(convo, param);});
	}else{
		set(convo, parseVal(param,'set'), text);
		let ans = findrep(parseVal(param,'ans'), convo);
		say(ans, convo, (convo) => {action(convo, parseVal(param,'next'));});
	}
  },{ typing: 1000 });
};

function buildButtons(params){
	var button = [];
	var data = [];
	var temp = parseNode(params, 'buttons');
	_.each(temp.Nodes, function(item, index) {
		var but = { type: parseVal(item,'type'), title: parseVal(item,'title'), payload: parseVal(item,'payload') };
		button.push(but);
		var dat = { respond: parseVal(item,'respond'), enumr: parseVal(item,'enumr'), next: parseVal(item,'next') };
		data.push(dat);
	});
	console.log(button);
	console.log(data);
	return { buttons: button, dat: data };
}

const askButtons = (convo, param) => {
	const data = buildButtons(param)
	 
	 
	let ask = findrep(parseVal(param,'ask'), convo);
   convo.ask((convo) => {   
    convo.sendButtonTemplate(ask, data.buttons, { typing: 1000 });
  }, (payload, convo, data) => {
		say('Proszę. Wciśnij przycisk.', convo, (convo) => {askButtons(convo, params);});

  }, [
    {
      event: 'postback',
      callback: (payload, convo) => {
		const ans = data.dat[Number(payload.postback.payload)];
		set(convo, parseVal(param,'set'),ans.enumr);
        say(ans.respond, convo, (convo) => {action(convo, ans.next);});
      }
    }
  ],{ typing: 1000 });
};


const askGender = (convo) => {
  convo.ask((convo) => {
    const buttons = [
      { type: 'postback', title: 'Male', payload: 'GENDER_MALE' },
      { type: 'postback', title: 'Female', payload: 'GENDER_FEMALE' },
      { type: 'postback', title: 'I don\'t wanna say', payload: 'GENDER_UNKNOWN' }
    ];
    convo.sendButtonTemplate(`Are you a boy or a girl?`, buttons);
  }, (payload, convo, data) => {
    const text = payload.message.text;
    convo.set('gender', text);
    convo.say(`Great, you are a ${text}`).then(() => askAge(convo));
  }, [
    {
      event: 'postback',
      callback: (payload, convo) => {
        convo.say('You clicked on a button').then(() => askAge(convo));
      }
    },
    {
      event: 'postback:GENDER_MALE',
      callback: (payload, convo) => {
        convo.say('You said you are a Male').then(() => askAge(convo));
      }
    },
    {
      event: 'quick_reply',
      callback: () => {}
    },
    {
      event: 'quick_reply:COLOR_BLUE',
      callback: () => {}
    },
    {
      pattern: ['yes', /yea(h)?/i, 'yup'],
      callback: () => {
        convo.say('You said YES!').then(() => askAge(convo));
      }
    }
  ]);
};

const askAge = (convo) => {
  convo.ask(`Ostatnie pytanie. Ile masz lat?`, (payload, convo, data) => {
    const text = payload.message.text;
    convo.set('age', text);
    say(`To wspaniale!`, convo, () => {
      say(`Ok, to są wszystkie informacje o Tobie:
      - Name: ${convo.get('name')}
      - Region: ${convo.get('region')}
      - Age: ${convo.get('age')}`,
	  convo,
	  () =>{convo.end();});
      
    });
  },{ typing: 1000 });
};

//bot.hear('hello', (payload, chat) => {
//  chat.conversation((convo) => {
//    convo.sendTypingIndicator(1000).then(() => askName(convo));
//  });
//});

//bot.hear('hey', (payload, chat) => {
//  chat.say('Hello friend', { typing: true }).then(() => (
//    chat.say('So, I’m good at talking about the weather. Other stuff, not so good. If you need help just enter “help.”', { typing: true })
//  ));
//});

//bot.hear('color', (payload, chat) => {
//  chat.say({
//    text: 'Favorite color?',
//   quickReplies: [ 'Red', 'Blue', 'Green' ]
//  });
//});

//bot.hear('image', (payload, chat) => {
//  chat.say({
//    attachment: 'image',
//    url: 'http://static3.gamespot.com/uploads/screen_medium/1365/13658182/3067965-overwatch-review-promo-20160523_v2.jpg',
//    quickReplies: [ 'Red', 'Blue', 'Green' ]
// });
//});

//bot.hear('button', (payload, chat) => {
//  chat.say({
//    text: 'Select a button',
//    buttons: [ 'Male', 'Female', `Don't wanna say` ]
//  });
//});

//bot.hear('convo', (payload, chat) => {
//  chat.conversation(convo => {
//    convo.ask({
//      text: 'Favorite color?',
//      quickReplies: [ 'Red', 'Blue', 'Green' ]
//    }, (payload, convo) => {
//      const text = payload.message.text;
//      convo.say(`Oh your favorite color is ${text}, cool!`);
//     convo.end();
//    }, [
//      {
//        event: 'quick_reply',
//        callback: (payload, convo) => {
//          const text = payload.message.text;
//          convo.say(`Thanks for choosing one of the options. Your favorite color is ${text}`);
//          convo.end();
//        }
//      }
//    ]);
//  });
//});

bot.start(process.env.PORT);
