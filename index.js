'use strict';
const BootBot = require('./lib/BootBot');

module.exports = BootBot;

'use strict';

const args = require('minimist')(process.argv.slice(2));

const https = require('http')

const GET_DB = function(path, resp){
	
	const options = {
  hostname: 'tidermondb.herokuapp.com',
  port: 80,
  path: path,
  method: 'GET',
   headers: {
    'Content-Type': 'application/json',
    'user': 'Wargamer',
	'db': 'apk'
  }
}

const req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', resp)
})

req.on('error', error => {
  console.error(error)
})

req.end()
}

const data = "todo=buy milk"

const POST_DB = function(path, data, resp){
	
const options = {
  hostname: 'tidermondb.herokuapp.com',
  port: 80,
  path: path,
  method: 'POST',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Content-Length': data.length,
	'user': 'Wargamer',
	'db': 'apk'
  }
}

const req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', resp)
})

req.on('error', error => {
  console.error(error)
})

req.write(data)
req.end()
} 

const DROP_DB = function(path, resp){
	
	
const options = {
  hostname: 'tidermondb.herokuapp.com',
  port: 80,
  path: path+"?action=drop",
  method: 'DELETE',
  headers: {
	'user': 'root',
	'db': 'apk'
  }
}

const req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', resp)
})

req.on('error', error => {
  console.error(error)
})


req.end()
} 
const TRIM_DB = function(path, resp){
	
const options = {
  hostname: 'tidermondb.herokuapp.com',
  port: 80,
  path: path+"?action=trim",
  method: 'DELETE',
  headers: {
	'user': 'root',
	'db': 'apk'
  }
}

const req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', resp)
})

req.on('error', error => {
  console.error(error)
})

req.end()
} 

if(args.m == "get"){
GET_DB(args.u, d => {
    process.stdout.write(d);
})
}
if(args.m == "post"){
POST_DB(args.u,args.d, d => {
    process.stdout.write(d); 
})
}
if(args.m == "drop"){
DROP_DB(args.u, d => {
    process.stdout.write(d);
})	
}
if(args.m == "trim"){
TRIM_DB(args.u, d => {
    process.stdout.write(d);
})	
}


